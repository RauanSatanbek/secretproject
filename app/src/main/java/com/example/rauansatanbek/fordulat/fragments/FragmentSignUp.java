package com.example.rauansatanbek.fordulat.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.example.rauansatanbek.fordulat.R;
import com.example.rauansatanbek.fordulat.databinding.FragmentSignupBinding;
import com.example.rauansatanbek.fordulat.models.user.UserSignUp;

public class FragmentSignUp extends Fragment implements AdapterView.OnItemSelectedListener, View.OnClickListener{
    FragmentSignupBinding fragmentSignupBinding;

    //user sign up model
    UserSignUp userSignUp;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentSignupBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_signup, container, false);
        View v = fragmentSignupBinding.getRoot();

        userSignUp = new UserSignUp();
        fragmentSignupBinding.setUserSignUp(userSignUp);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.gender_array, R.layout.custom_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //spinner gender
        fragmentSignupBinding.spinnerGender.setAdapter(adapter);

        //button sign up
        fragmentSignupBinding.btnSignUp.setOnClickListener(this);
        return v;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        userSignUp.setGender(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        Log.d("MyLogs", "onClick: - name: " + userSignUp.name + "\n" +
                "email: " + userSignUp.email + "\n" +
                "password: " + userSignUp.password + "\n" +
                "gender: " + userSignUp.gender );
    }
}
