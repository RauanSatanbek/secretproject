package com.example.rauansatanbek.fordulat.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.rauansatanbek.fordulat.R;
import com.example.rauansatanbek.fordulat.databinding.ActivityLoginBinding;
import com.example.rauansatanbek.fordulat.fragments.FragmentSignUp;

public class LoginActivity extends AppCompatActivity {

    ActivityLoginBinding activityLoginBinding;
    View mDecorView;

    //fragment transaction
    FragmentTransaction fragmentTransaction;
    FragmentSignUp fragmentSignUp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        mDecorView = getWindow().getDecorView();


        //fragment sign up
        fragmentSignUp = new FragmentSignUp();
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction
                .add(R.id.login_container, fragmentSignUp)
                .commit();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            mDecorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);}
    }
}
