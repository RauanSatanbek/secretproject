package com.example.rauansatanbek.fordulat.models.user;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.rauansatanbek.fordulat.BR;

public class UserSignUp extends BaseObservable {
    public String name;
    public String email;
    public String password;
    public int gender;

    @Bindable
    public String getName() {
        return this.name;
    }

    @Bindable
    public String getEmail() {
        return this.email;
    }


    @Bindable
    public String getPassword() {
        return this.password;
    }

    @Bindable
    public int getGender() {
        return this.gender;
    }

    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.name);
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }

    public void setPassword(String password) {
        this.password = password;
        notifyPropertyChanged(BR.password);
    }

    public void setGender(int gender) {
        this.gender = gender;
        notifyPropertyChanged(BR.gender);
    }
}
